import { Injectable } from '@nestjs/common';
import { CreateServeDto } from './dto/create-serve.dto';
import { UpdateServeDto } from './dto/update-serve.dto';

@Injectable()
export class ServeService {
  create(createServeDto: CreateServeDto) {
    return 'This action adds a new serve';
  }

  findAll() {
    return `This action returns all serve`;
  }

  findOne(id: number) {
    return `This action returns a #${id} serve`;
  }

  update(id: number, updateServeDto: UpdateServeDto) {
    return `This action updates a #${id} serve`;
  }

  remove(id: number) {
    return `This action removes a #${id} serve`;
  }
}
